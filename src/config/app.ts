import express, { Application } from 'express';
import path from 'path';

const webRoutes = require('./../routes/web');
const apiRoutes = require('./../routes/api');
class App {
  public app: Application;
  public port: number;

  constructor(appInit: { port: number; middleWares: any }) {
    this.app = express();
    this.port = appInit.port;

    this.middlewares(appInit.middleWares);
    this.routes();
    this.assets();
  }

  private middlewares(middleWares: {
    forEach: (arg0: (middleWare: any) => void) => void;
  }) {
    middleWares.forEach(middleWare => {
      this.app.use(middleWare);
    });
  }

  private routes() {
    this.app.use('/', webRoutes);
    this.app.use('/api', apiRoutes);
  }

  private assets() {
    this.app.use(express.static(path.join(__dirname, 'public')));
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.log(`App listening on the http://localhost:${this.port}`);
    });
  }
}

export default App;

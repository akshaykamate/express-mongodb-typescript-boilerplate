import express from 'express';
const pj = require('./../../../package.json');

let router = express.Router();

/* GET home page. */
router.get('/', function (req, res: any) {
  res.send(
    'Express version (' + pj.dependencies.express + ') (NodeJS framework)'
  );
});

module.exports = router;

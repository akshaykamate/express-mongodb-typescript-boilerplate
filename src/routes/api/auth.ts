import express from 'express';
import AuthController from './../../controller/AuthController';

let router = express.Router();
const auth = new AuthController();

// router.post("/register", auth.register);
router.post('/login', auth.login);
// router.post("/verify-otp", auth.verifyConfirm);
// router.post("/resend-verify-otp", auth.resendConfirmOtp);

module.exports = router;

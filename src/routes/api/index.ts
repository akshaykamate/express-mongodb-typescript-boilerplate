import express from 'express';

const authRouter = require('./auth');
var app = express();
app.use('/auth/', authRouter);

module.exports = app;

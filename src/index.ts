import App from './config/app';

import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import cors from 'cors';

const app = new App({
  port: 5000,
  middleWares: [
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    cookieParser(),
    morgan('dev'),
    cors(),
  ],
});

app.listen();

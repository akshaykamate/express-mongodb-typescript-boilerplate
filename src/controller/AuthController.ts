// import bcrypt from "bcrypt";
// import * as jwt from "jsonwebtoken";
import validator from 'validator';
import { Request, Response } from 'express';

// const UserModel = require("../models/UserModel");
const apiResponse = require('../helpers/apiResponse');
// const utility = require("../helpers/utility");

class AuthController {
  constructor() {}
  login(req: Request, res: Response) {
    let errors = {
      email: '',
      password: '',
    };
    let valid = true;
    const values = req.body;
    if (!validator.isEmail(values.email)) {
      valid = false;
      errors.email = 'Invalid Email Address';
    }
    if (!validator.isByteLength(values.password, { min: 6, max: 32 })) {
      valid = false;
      errors.password = 'Invalid passwords';
    }
    console.log(valid);
    // try {
    if (valid) {
      return res.status(200).send(req.body);
    } else {
      return apiResponse.validationErrorWithData(
        res,
        'Validation Error.',
        errors
      );
    }
    // } catch (err) {
    //   return res.status(500).send(err);
    // }
  }
}

export default AuthController;
